﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jheison_Mendoza_Leccion
{
    public class ConcreteBuilder : IBuilder
    {
        //Creación de objeto.
        private Product hamburguesa = new Product();

        // Una nueva instancia del constructor debe contener un objeto de producto en blanco.
        public ConcreteBuilder()
        {
            this.Reset();
        }

        public void Reset()
        {
            this.hamburguesa = new Product();
        }

        // Todos los pasos de producción funcionan con la misma instancia de producto.
        public void BuildEspecial()
        {
            this.hamburguesa.Add("Hamburguesa Especial");
        }

        public void BuildStickBurger()
        {
            this.hamburguesa.Add("Hamburguesa Stick");
        }

        public void BuildAmericana()
        {
            this.hamburguesa.Add("Hamburguesa Americana");
        }

        public void BuilParrillera()
        {
            this.hamburguesa.Add("Hamburguesa Parrillera");
        }

        public void BuildManaba()
        {
            this.hamburguesa.Add("Hamburguesa Manaba");
        }

        public void BuildRanchoBurger()
        {
            this.hamburguesa.Add("Hamburguesa Rancho");
        }

        public void BuildHawaina()
        {
            this.hamburguesa.Add("Hamburguesa Hawaina");
        }


        //Sera párte para la Mostrar lo datos de la hamburguesa
        public Product GetProduct()
        {
            Product result = this.hamburguesa;

            this.Reset();

            return result;
        }

  
    }
}
