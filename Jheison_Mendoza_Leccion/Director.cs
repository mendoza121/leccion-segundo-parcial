﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jheison_Mendoza_Leccion
{
    public class Director 
    {
        //Declaración de variable
        private IBuilder _builder;
        //Declaración de métodos
        public IBuilder Builder
        {
            set { _builder = value; }
        }

        // El Director puede construir varias variaciones de productos usando el mismo
        // pasos de construcción.
        public void BuildMinimalViableProduct()
        {
            this._builder.BuildEspecial();
        }

        public void BuildFullFeaturedProduct()
        {
            this._builder.BuildEspecial();
            this._builder.BuildStickBurger();
            this._builder.BuildAmericana();
            this._builder.BuilParrillera();
            this._builder.BuildManaba();
            this._builder.BuildRanchoBurger();
            this._builder.BuildHawaina();

        }

    }
}
