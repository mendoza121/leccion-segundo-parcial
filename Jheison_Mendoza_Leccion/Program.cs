﻿using System;

namespace Jheison_Mendoza_Leccion
{
    class Program
    {
        static void Main(string[] args)
        {
            //Los patrones Builder (o Constructor) facilitan a los desarrolladores el proceso de programación
            //porque no han de rediseñar cada paso que se repite como una rutina de programa.


            //Creación de objetos constructor
            var director = new Director();
            var builder = new ConcreteBuilder();
            director.Builder = builder;

            //Creación de objetos basicos
            Console.WriteLine("Hamburguesa básica estándar:");
            director.BuildMinimalViableProduct();
            Console.WriteLine(builder.GetProduct().ListParts());

            //Creacion de Objetos Estandar
            Console.WriteLine("Hamburguesa estándar completa:");
            director.BuildFullFeaturedProduct();
            Console.WriteLine(builder.GetProduct().ListParts());

            //Creación de Objetos personalizado
            Console.WriteLine("Hamburguesa personalizada:");
            builder.BuildEspecial();
            builder.BuildStickBurger();
            builder.BuildAmericana();
            builder.BuilParrillera();
            builder.BuildManaba();



            Console.Write(builder.GetProduct().ListParts());





        }
    }
}
