﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jheison_Mendoza_Leccion
{
    public class Product
    {
        //Lista Para Guardar Los Componentes De La Clase Principal es decir la Hamburguesa
        private List<object> ListaHamburguesa = new List<object>();

        public void Add(string part)
        {
            this.ListaHamburguesa.Add(part);
        }

        //Metodo que muestra los Componentes
        public string ListParts()
        {
            string str = string.Empty;

            for (int i = 0; i < this.ListaHamburguesa.Count; i++)
            {
                str += this.ListaHamburguesa[i] + ", ";
            }

            str = str.Remove(str.Length - 2); 

            return "Producto estándar completo: " + str + "\n";

        }
    }
}
