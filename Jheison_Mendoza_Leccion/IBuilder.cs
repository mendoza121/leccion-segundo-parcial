﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jheison_Mendoza_Leccion
{
    public interface IBuilder
    {
        //Metodos implemetado en la interfaz IBuilder necesarios para el proyecto
        void BuildEspecial();

        void BuildStickBurger();

        void BuildAmericana();

        void BuilParrillera();

        void BuildManaba();

        void BuildRanchoBurger();

        void BuildHawaina();
    }
}
